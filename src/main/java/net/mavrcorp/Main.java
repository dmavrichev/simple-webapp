package net.mavrcorp;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

@Path("/dmma")
public class Main {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String test() {
        return "Hello World";
    }

    @POST
    public Response dmma2() {
        Person dmitry = new Person("Dmitry", 27, new Date());
        return Response.ok(dmitry, MediaType.APPLICATION_JSON_TYPE).build();
    }
}
